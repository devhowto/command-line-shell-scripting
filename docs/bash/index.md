# Bash

Bash, the GNU Bourne Again Shell!

```{toctree}
:maxdepth: 6

bash-completion
bash-parameter-expansion
```
