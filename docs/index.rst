Command Line and Shell Scripting
================================

.. toctree::
   :hidden:
   :maxdepth: 6
   :caption: Contents:

   cmdline/index
   bash/index


May the source be with you!
---------------------------

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
