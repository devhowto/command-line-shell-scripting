# Command Line and Shell Scripting

Explanations, examples, tips and notes on using the command line and shell scripting on Unix-like systems.

* [Intro](#intro)
* [Local Development](#local-development)

## Intro

Read the documentation:

- https://command-line-and-shell-scripting.readthedocs.io

**NOTE**: The main repository is [this one on Gitlab](https://gitlab.com/devhowto/command-line-shell-scripting.git). The Github one is a mirror repo and not to be used to report issues and pull requests.

The goal of this project is to serve as a wiki of sorts about all things command line. We intend to explain concepts and idea, but also and equally important, to show actual real and useful examples of command line and shell scripting.

## Local Development

Install the dependencies (you need to do this only once or when we update or add new dependencies).

See the link below on how to install Sphinx for your operating system:

- https://www.sphinx-doc.org/en/master/usage/installation.html

We use the theme [Furo](https://pradyunsg.me/furo/) and write using Markdown (not reStructuredText), so we also need [myst-parser](https://myst-parser.readthedocs.io/en/latest/). These are also required in ReadThedocs, it seems. These are required for ReadTheDocs and for locally running the project as well. `cd` to the root directory of this project and run:

```text
$ pip install --requirement docs/requirements.txt
```

Finally, run the command that watches for file changes and serves the project:

```text
$ make develop
```

From the browser, visit http://localhost:4321.

We can also add this to `/etc/hosts`:

```text
127.0.0.1 local.cmdline.dev
```

So we can access it as http://local.cmdline.dev:4321 as well.

**NOTE**: It looks like ReadTheDocs **requires** documentation to be in a `docs/` directory. Therefore, we have our documents inside such directory. Still, you can run `make develop` from the root directory of the git repository, and not from inside `docs/` directory. Example:

```text
~/work/src/cmdline-shell-script [main *+% u+1]
$ tree -aCF -L 1 .
.
├── .git/
├── .gitignore
├── Makefile
├── README.md
├── docs/
└── make.bat

$ make develop
```

Note we are inside a directory that *contains* `docs/`, not inside `docs/` itself.

